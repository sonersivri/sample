package io.mavinci.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * <b>Project confserverui</b><br />
 * AppConfig.java<br />
 *
 * <b>created at</b> 20 Ara 2014 15:42:55
 * @author soners
 * @since 1.00.0
 */
@Configuration
@Import({ SoapConfig.class, MvcConfig.class, SwaggerConfig.class })
public class AppConfig
{
}