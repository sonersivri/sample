package io.mavinci.api.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * <b>Project confserverui</b><br />
 * SpringMvcInitializer.java<br />
 *
 * <b>created at</b> 20 Ara 2014 15:47:59
 * @author soners
 * @since 1.00.0
 */
public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	/**
	 * SpringMvcInitializer<br />
	 * 
	 * @return
	 * 
	 * <b>created at</b> 20 Ara 2014 15:48:02
	 * @since 1.00.0
	 * @author soners
	 */
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { AppConfig.class };
	}

	/**
	 * SpringMvcInitializer<br />
	 * 
	 * @return
	 * 
	 * <b>created at</b> 20 Ara 2014 15:48:05
	 * @since 1.00.0
	 * @author soners
	 */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	/**
	 * SpringMvcInitializer<br />
	 * 
	 * @return
	 * 
	 * <b>created at</b> 20 Ara 2014 15:48:08
	 * @since 1.00.0
	 * @author soners
	 */
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}
	
}