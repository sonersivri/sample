package io.mavinci.api.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * <b>Project confserverui</b><br />
 * MvcConfig.java<br />
 *
 * <b>created at</b> 3 Ara 2014 09:52:45
 * @author soners
 * @since 1.00.0
 */
@Configuration
@ComponentScan({ "io.mavinci.service" })
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter
{

	private static final Logger LOGGER = LoggerFactory.getLogger(MvcConfig.class);
	/**
	 * MvcConfig<br />
	 * 
	 * @param configurer
	 * 
	 * <b>created at</b> 3 Ara 2014 09:52:48
	 * @since 1.00.0
	 * @author soners
	 */
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer)
	{
		LOGGER.info("soner sivri");
		configurer.enable();
	}

	/**
	 * MvcConfig<br />
	 * 
	 * @param registry
	 * 
	 * <b>created at</b> 3 Ara 2014 09:52:50
	 * @since 1.00.0
	 * @author soners
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry)
	{
	}

}
