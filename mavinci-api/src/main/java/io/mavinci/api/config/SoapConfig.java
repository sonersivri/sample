package io.mavinci.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.jaxws.SimpleJaxWsServiceExporter;

@Configuration
//@ImportResource("classpath:/soap.xml")
public class SoapConfig {
	@Bean
	public SimpleJaxWsServiceExporter exporter() {
		SimpleJaxWsServiceExporter simpleJaxWsServiceExporter = new SimpleJaxWsServiceExporter();
		simpleJaxWsServiceExporter.setBaseAddress("http://0.0.0.0:8081/");
		return simpleJaxWsServiceExporter;
	}
}
