package io.mavinci.api.config;

import org.jasig.cas.client.validation.Cas20ServiceTicketValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.security.cas.ServiceProperties;
import org.springframework.security.cas.authentication.CasAuthenticationProvider;
import org.springframework.security.cas.web.CasAuthenticationEntryPoint;
import org.springframework.security.cas.web.CasAuthenticationFilter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
//	@Bean
//	public ServiceProperties serviceProperties() {
//		ServiceProperties serviceProperties = new ServiceProperties();
//		serviceProperties.setService("https://localhost:8443/cas-sample/j_spring_cas_security_check");
//		serviceProperties.setSendRenew(false);
//		return serviceProperties;
//	}
//
//	@Bean
//	public CasAuthenticationProvider casAuthenticationProvider() {
//		CasAuthenticationProvider casAuthenticationProvider = new CasAuthenticationProvider();
//		casAuthenticationProvider.setAuthenticationUserDetailsService(authenticationUserDetailsService());
//		casAuthenticationProvider.setServiceProperties(serviceProperties());
//		casAuthenticationProvider.setTicketValidator(cas20ServiceTicketValidator());
//		casAuthenticationProvider.setKey("an_id_for_this_auth_provider_only");
//		return casAuthenticationProvider;
//	}
//
//	@Bean
//	public AuthenticationUserDetailsService authenticationUserDetailsService() {
//		return new TestCasAuthenticationUserDetailsService();
//	}
//
//	@Bean
//	public Cas20ServiceTicketValidator cas20ServiceTicketValidator() {
//		return new Cas20ServiceTicketValidator("https://localhost:9443/cas");
//	}
//
//	@Bean
//	public CasAuthenticationFilter casAuthenticationFilter() throws Exception {
//		CasAuthenticationFilter casAuthenticationFilter = new CasAuthenticationFilter();
//		casAuthenticationFilter.setAuthenticationManager(authenticationManager());
//		return casAuthenticationFilter;
//	}
//
//	@Bean
//	public CasAuthenticationEntryPoint casAuthenticationEntryPoint() {
//		CasAuthenticationEntryPoint casAuthenticationEntryPoint = new CasAuthenticationEntryPoint();
//		casAuthenticationEntryPoint.setLoginUrl("https://localhost:9443/cas/login");
//		casAuthenticationEntryPoint.setServiceProperties(serviceProperties());
//		return casAuthenticationEntryPoint;
//	}
//
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		http.addFilter(casAuthenticationFilter());
//		http.exceptionHandling().authenticationEntryPoint(casAuthenticationEntryPoint());
//	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.authenticationProvider(casAuthenticationProvider());
	}
}
