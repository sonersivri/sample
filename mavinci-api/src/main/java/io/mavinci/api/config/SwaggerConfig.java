package io.mavinci.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.google.common.collect.Lists;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <b>Project motat</b><br />
 * SwaggerConfig.java<br />
 *
 * <b>created at</b> 2016-07-28 18:40:30
 * @author soners
 * @since 1.3.0
 */
@Configuration
@EnableSwagger2
//@Profile("swagger")
public class SwaggerConfig
{
	private static String TOKEN = "token";
	/**
	 * SwaggerConfig<br />
	 * 
	 * @return
	 * 
	 * <b>created at</b> 2016-07-28 18:40:33
	 * @since 1.3.0
	 * @author soners
	 */
	@Bean
	public Docket api()
	{
		return new Docket(DocumentationType.SWAGGER_2)
		        .select()
		        .apis(RequestHandlerSelectors.basePackage("io.mavinci"))
		        .paths(PathSelectors.any())
		        .build()
		        .apiInfo(this.apiInfo())
				.globalOperationParameters(Lists.newArrayList(
						new ParameterBuilder().name(TOKEN).description("Access Token").modelRef(new ModelRef("string"))
								.parameterType("header").required(false).allowMultiple(true).defaultValue("").build()));
	}

	/**
	 * SwaggerConfig<br />
	 * 
	 * @return
	 * 
	 * <b>created at</b> 2016-07-29 19:02:22
	 * @since 1.3.0
	 * @author soners
	 */
	@Bean
	public ApiInfo apiInfo()
	{
		ApiInfoBuilder builder = new ApiInfoBuilder();
		builder.title("NG Services - API Reference");
		builder.version("3.0.0");
		builder.description(
				"This API Reference is awesome!<hr />" + "<div><input class=\"parameter\" id=\"global-token\" type=\"text\"> "
						+ "<button onclick=\"$('input[name=\\'"+ TOKEN + "\\']').val($('#global-token').val())\">Set global token</button> "
						+ "<button onclick=\"$('input[name=\\'" + TOKEN + "\\']').val('')\">Clear global token</button>"
						+ "</div>");
		return builder.build();

	}

}