package io.mavinci.service.controller;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
	@PostMapping("test")
	public ResponseEntity<Map<String, String>> test(@RequestBody Map<String, String> inputMap)
	{
		
		return new ResponseEntity<>(inputMap, HttpStatus.OK);
	}
}